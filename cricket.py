#!/bin/python3
import random

num = ["0","1","2","3","4","5","6","7","8","9","10"]
opt = ["Bat","Ball"]

def comp():
    nm = random.choice(num)
    return int(nm)

def toss_result(p1,call):
    a = 11
    while(a > 10):
        pinp = int(input("Enter a value in range 1 to 10 for toss: "))
        a = pinp

    cpi = comp()

    total = pinp + cpi

    if call == "Even":
        if total % 2 == 0:
            return f"{p1} won"
        else:
            return "computer won"
    elif call == "Odd":
        if total % 2 == 1:
            return f"{p1} won"
        else:
            return "computer won"

def choice(r,p1):
    if r == f'{p1} won':
        print(r.capitalize())
        c = "ch"
        while(c=="ch"):
            chie = str(input("Bat or Ball: ")).capitalize()
            if chie == "Bat" or chie == "Ball":
                c = "ok"
                return chie
    else:
        print(r.capitalize())
        chie = random.choice(opt)
        return chie

def bat_ball(who,p1,i1:int):
    status = "NO"
    run = 0
    if who == f"{p1}":
        print("\n You are Batting \n")
        if i1 == 99999999999:
            print("Target to be set")
        else:
            print(f"Target = {i1}")
        while(status == "NO"):
            hand = int(input("Enter a number between 0 to 10: "))
            chand = comp()
        
            if chand == hand:
                status = "out"
                print("You are out")
                return int(run)
            elif hand >= 0 and hand <=10:
                run += hand
                if i1 < run:
                    status = "won"
                    return run
                else:
                    print(f"You scored {hand} and total score is {run}")
            else:
                print("\nWhat part of 'between 1 and 10' you don't get?\n")
            
    else:
        print("\n You are Bowling \n")
        if i1 == 99999999999:
            print("Target to be set")
        else:
            print(f"Target = {i1}")
        while(status == "NO"):
            hand = int(input("Enter a number between 0 to 10: "))
            chand = comp()
        
            if chand == hand:
                status = "out"
                print("Comp is out")
                return int(run)
            elif chand >= 0 and chand <= 10:
                run += chand
                if i1 < run:
                    status = "won"
                    return run
                else:
                    print(f"Comp scored {chand} and total score is {run}")
            else:
                print("\nWhat part of 'between 1 and 10' you don't get?\n")
            
def result(sp,sc):
    if sp == sc:
        return "Tie"
    elif sp > sc:
        return "You Won"
    else:
        return "You Lost"

def game():
    p1 = str(input("ENTER YOUR NAME: "))
    toss_call = str(input("Call\nODD or EVEN: ")).capitalize()

    r = toss_result(p1,toss_call)
    bob = choice(r,p1)
    print(f" And choose to {bob}\n")
    inn = "Running"
    while(inn == "Running"):
        if r == f'{p1} won':
            if bob == "Bat":
                runs = bat_ball(p1,p1,99999999999)
                cruns = bat_ball("comp",p1,runs)
            else:
                cruns = bat_ball("comp",p1,99999999999)
                runs = bat_ball(p1,p1,cruns)
        else:
            if bob == "Bat":
                cruns = bat_ball("comp",p1,99999999999)
                runs = bat_ball(p1,p1,cruns)
            else:
                runs = bat_ball(p1,p1,99999999999)
                cruns = bat_ball("comp",p1,runs)

        rsul = result(runs,cruns)
        inn = "done"
    print(rsul)

game()